/* -*- mode: C; c-basic-offset: 2 -*- */
/* Copyright (C) 2021 Damien Caliste <damien.caliste@cea.fr>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

/**
 * @file check_input.c
 * @brief checks input.c and input.h
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <check.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include "input.h"
#include "pspio_common.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

static char sample[] = "Hello\n"
    "\n"
    "I'm a test file to see if we can read lines.\n"
    "Including small lines.\n"
    "And very long ones --------------------------------------------------"
    "------------------------------------------------------------"
    "------------------------------------------------------------"
    "------------------------------------------------------------"
    "-----------------------++++++++\n"
    "  with some space   \n"
    "\n"
    "and no trailing carriage return.";

START_TEST(test_input_file)
{
  FILE *file;
  input_t *input;

  file = fopen("tmp-input", "w");
  ck_assert(fputs(sample, file) != EOF);
  fclose(file);

  input = NULL;
  ck_assert(input_new_from_filename(&input, "tmp-input") == PSPIO_SUCCESS);

  ck_assert_str_eq(input_get_line(input), "Hello\n");
  ck_assert_str_eq(input_get_line(input), "\n");
  input_push_position(input);
  ck_assert_str_eq(input_get_line(input), "I'm a test file to see if we can read lines.\n");
  ck_assert_str_eq(input_get_line(input), "Including small lines.\n");
  ck_assert_str_eq(input_get_line(input), "And very long ones --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
  ck_assert_str_eq(input_get_line(input), "-----------------++++++++\n");
  ck_assert_str_eq(input_get_line(input), "  with some space   \n");
  ck_assert_str_eq(input_get_line(input), "\n");
  ck_assert_str_eq(input_get_line(input), "and no trailing carriage return.");
  ck_assert(input_get_line(input) == NULL);

  input_pop_position(input);
  ck_assert_str_eq(input_get_line(input), "I'm a test file to see if we can read lines.\n");

  input_rewind(input);
  ck_assert_str_eq(input_get_line(input), "Hello\n");

  input_free(input);
  
  ck_assert(unlink("tmp-input") == 0);
}
END_TEST

START_TEST(test_input_buffer)
{
  input_t *input;

  input = NULL;
  ck_assert(input_new_from_buffer(&input, sample) == PSPIO_SUCCESS);

  ck_assert_str_eq(input_get_line(input), "Hello");
  ck_assert_str_eq(input_get_line(input), "");
  input_push_position(input);
  ck_assert_str_eq(input_get_line(input), "I'm a test file to see if we can read lines.");
  ck_assert_str_eq(input_get_line(input), "Including small lines.");
  ck_assert_str_eq(input_get_line(input), "And very long ones -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------++++++++");
  ck_assert_str_eq(input_get_line(input), "  with some space   ");
  ck_assert_str_eq(input_get_line(input), "");
  ck_assert_str_eq(input_get_line(input), "and no trailing carriage return.");
  ck_assert(input_get_line(input) == NULL);

  input_pop_position(input);
  ck_assert_str_eq(input_get_line(input), "I'm a test file to see if we can read lines.");

  input_rewind(input);
  ck_assert_str_eq(input_get_line(input), "Hello");

  input_free(input);
}
END_TEST

START_TEST(test_input_pipe)
{
  pid_t pid;
  int fd[2];
  ssize_t offset;
  size_t ln = strlen(sample);

  ck_assert(pipe(fd) >= 0);

  pid = fork();
  ck_assert(pid >= 0);
  if (pid > 0) {
    /* Child case. */
    input_t *input;

    input = NULL;
    ck_assert(input_new_from_file_descriptor(&input, fd[0]) == PSPIO_SUCCESS);
    close(fd[1]);

    ck_assert_str_eq(input_get_line(input), "Hello");
    ck_assert_str_eq(input_get_line(input), "");
    input_push_position(input);
    ck_assert_str_eq(input_get_line(input), "I'm a test file to see if we can read lines.");
    ck_assert_str_eq(input_get_line(input), "Including small lines.");
    ck_assert_str_eq(input_get_line(input), "And very long ones --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    ck_assert_str_eq(input_get_line(input), "-----------------++++++++");
    ck_assert_str_eq(input_get_line(input), "  with some space   ");
    ck_assert_str_eq(input_get_line(input), "");
    ck_assert_str_eq(input_get_line(input), "and no trailing carriage return.");
    ck_assert(input_get_line(input) == NULL);

    /* Pop and rewind will not work at the moment, because
       the pipe is not supporting rewind. A possible solution
       to make the feature working for pipes also would be to cache
       the received bits. Before that, the two tests below are
       commented because they are expected to fail. */
    input_pop_position(input);
    /* ck_assert_str_eq(input_get_line(input), "I'm a test file to see if we can read lines."); */

    input_rewind(input);
    /* ck_assert_str_eq(input_get_line(input), "Hello"); */

    input_free(input);
    close(fd[0]);
  } else if (!pid) {
    /* Parent case. */
    close(fd[0]);
    offset = 0;
    while (sample[offset]) {
      size_t n = ln - offset;
      ssize_t nbuf;
      nbuf = write(fd[1], sample + offset, n < 5 ? n : 5);
      if (nbuf < 0) {
        offset = ln;
      } else {
        offset += nbuf;
      }
    };
    close(fd[1]);
  }
}
END_TEST

Suite * make_input_suite(void)
{
  Suite *s;
  TCase *tc_input;

  s = suite_create("Input");

  tc_input = tcase_create("Read sample data");
  tcase_add_test(tc_input, test_input_file);
  tcase_add_test(tc_input, test_input_buffer);
  tcase_add_test(tc_input, test_input_pipe);
  suite_add_tcase(s, tc_input);

  return s;
}
