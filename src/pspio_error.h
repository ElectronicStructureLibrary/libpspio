/* Copyright (C) 2011-2016 Joseba Alberdi <alberdi@hotmail.es>
 *                         Matthieu Verstraete <matthieu.jean.verstraete@gmail.com>
 *                         Micael Oliveira <micael.oliveira@mpsd.mpg.de>
 *                         Yann Pouillon <devops@materialsevolution.es>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

#ifndef PSPIO_ERROR_H
#define PSPIO_ERROR_H

/**
 * @file pspio_error.h 
 * @brief Error handlers
 */

#include <stdlib.h>
#include <stdio.h>

#include "pspio_common.h"


/**********************************************************************
 * Data structures                                                    *
 **********************************************************************/

/**
 * Global error handling structure.
 * It's an opaque structure without any public field.
 */
typedef struct pspio_error_type pspio_error_t;


/**********************************************************************
 * Routines                                                           *
 **********************************************************************/

/**
 * Add an error to the chain.
 * @param[in] error_id: error code.
 * @param[in] filename: source filename (use NULL if none).
 * @param[in] line: line number in the source file (ignored if filename
 *            is NULL).
 * @param[in] routine: current routine in the source file.
 * @return the error code provided as input (for automation purposes).
 */
int pspio_error_add(int error_id, const char *filename, int line,
		    const char *routine);

/**
 * Fetch and clear the error chain.
 * @return string pointer describing the chain of errors.
 */
char *pspio_error_fetchall(void);

/**
 * Flush and clear the error chain.
 * @param[in] fd: file descriptor for the output.
 */
void pspio_error_flush(FILE *fd);

/**
 * Clear the error chain.
 */
void pspio_error_free(void);

/**
 * Clear the given error.
 */
void pspio_error_free_one(pspio_error_t *err);

/**
 * Get the current error status.
 * @param[in] routine: return last error of the specified routine, or the very 
 *            last error if NULL.
 * @return error code
 */
int pspio_error_get_last(const char *routine);

/**
 * Get the length of the error chain.
 * @return length of the error chain
 */
int pspio_error_len(void);

/**
 * Pop the first available error.
 * @return error structure pointer. Should call pspio_error_free_pop()
 * to free it after use.
 */
pspio_error_t *pspio_error_pop(void);

/**
 * Displays an error message.
 * @param[in] error_id: integer identifying the error.
 * @param[in] filename: source filename (use NULL if none).
 * @param[in] line: line number in the source file (ignored if filename
 *            is NULL).
 * @param[in] routine: current routine in the source file.
 * @return string with error message.
 */
void pspio_error_show(int error_id, const char *filename, int line,
		      const char *routine);

/**
 * Returns a string with error description.
 * @param[in] pspio_errorid: integer identifying the error.
 * @return string with error message.
 */
const char *pspio_error_string(const int error_id);


/**********************************************************************
 * Macros                                                             *
 **********************************************************************/

/**
 * Macro to break loops when there are deferred errors, in order to propagate
 * error conditions from inner loops to outer loops
 */
#define BREAK_ON_DEFERRED_ERROR \
  if ( pspio_error_get_last(__func__) != PSPIO_SUCCESS ) { \
    break; \
  }

/**
 * Deferred error handler macro for function calls, when it is necessary
 * to continue executing the code despite the error
 * @param[in] function_call: the function to be called with all its parameters
 */
#define DEFER_FUNC_ERROR(function_call) \
  pspio_error_add(function_call, __FILE__, __LINE__, __func__);

/**
 * Deferred error handler macro for unsatisfied conditions, when it is
 * necessary to continue executing the code despite the error
 * @param[in] condition: condition to check
 * @param[in] error_id: error code to set
 */
#define DEFER_TEST_ERROR(condition, error_id) \
  if ( !(condition) ) {		    \
    pspio_error_add(error_id, __FILE__, __LINE__, __func__); \
  }

/**
 * Deferred error handler macro for failing allocations, when it is
 * necessary to continue executing the code despite the error.
 * @param[in] T: the variable to allocate
 * @param[in] K: the kind of T
 * @param[in] S: the number of elements of T
 */
#define DEFER_ALLOC_ERROR(T, K, S) \
  if ( !(T = (K*)malloc(S * sizeof(K))) ) {  \
    pspio_error_add(PSPIO_ENOMEM, __FILE__, __LINE__, __func__);     \
  }

/**
 * Allocation macro to be used in *_alloc() routines. It requires to
 * include "string.h" to be used, because it is nullifying all the
 * structure bits.
 * @param[in] T: the type name.
 */
#define ALLOC_OR_RETURN(T)                                              \
  *T = (pspio_##T##_t *)malloc(sizeof(pspio_##T##_t));                  \
  if (*T == NULL) {                                                     \
    return pspio_error_add(PSPIO_ENOMEM, __FILE__, __LINE__, __func__); \
  }                                                                     \
  memset(*T, '\0', sizeof(pspio_##T##_t));

/**
 * Error handler macro for deferred errors in *_alloc() routines. If
 * an error occured before this macro, the type is freed and the
 * pointer to it is nullified.
 * @param[in] T: the type used in *_alloc().
 */
#define CLEAN_AND_RETURN_ON_DEFERRED_ERROR(T)              \
  if ( pspio_error_get_last(__func__) != PSPIO_SUCCESS ) { \
    pspio_##T##_free(*T);                                  \
    *T = NULL;                                             \
    return pspio_error_get_last(__func__);                 \
  }

/**
 * Error handler macro for deferred errors requiring to clean local
 * memory before leaving.
 * @param[in] JOB: the part of code to be executed to clean local memory.
 */
#define EXE_AND_RETURN_ON_DEFERRED_ERROR(JOB)              \
  if ( pspio_error_get_last(__func__) != PSPIO_SUCCESS ) { \
    JOB;                                                   \
    return pspio_error_get_last(__func__);                 \
  }

/**
 * Macro to break out of a loop when a condition is unsatisfied
 * @param[in] condition: condition to check
 * @param[in] error_id: error code to set before breaking out of the loop
 */
#define FULFILL_OR_BREAK(condition, error_id) \
  if (!(condition)) { \
    pspio_error_add(error_id, __FILE__, __LINE__, __func__); \
    break; \
  }

/**
 * Macro to return from a routine when a condition is unsatisfied
 * @param[in] condition: condition to check
 * @param[in] error_id: error code to set before returning
 */
#define FULFILL_OR_RETURN(condition, error_id) \
  if (!(condition)) {		    \
    return pspio_error_add(error_id, __FILE__, __LINE__, __func__); \
  }

/**
 * Error handler macro for deferred errors
 */
#define RETURN_ON_DEFERRED_ERROR \
  if ( pspio_error_get_last(__func__) != PSPIO_SUCCESS ) { \
    return pspio_error_get_last(__func__); \
  }

/**
 * Error handler macro ensuring that errors are properly registered
 * before returning from a function
 * @param[in] error_id: error code
 */
#define RETURN_WITH_ERROR(error_id) \
    return pspio_error_add(error_id, __FILE__, __LINE__, __func__);

/**
 * Break the current loop when a pspio function call fails
 * @param[in] function_call: the function to be called with all its parameters
 */
#define SUCCEED_OR_BREAK(function_call) \
  if ( pspio_error_add(function_call, __FILE__, __LINE__, __func__) != PSPIO_SUCCESS ) { \
    break; \
  }

/**
 * Return when a pspio function call fails
 * @param[in] function_call: the function to be called with all its parameters
 */
#define SUCCEED_OR_RETURN(function_call) \
  if ( pspio_error_add(function_call, __FILE__, __LINE__, __func__) != PSPIO_SUCCESS ) { \
    return pspio_error_get_last(NULL); \
  }

/**
 * Macro to skip routine execution on error
 * @param[in] routine_call: the routine to be called with all its parameters
 */
#define SKIP_CALL_ON_ERROR(routine_call) \
  if ( pspio_error_get_last(__func__) == PSPIO_SUCCESS ) { \
    routine_call; \
  }

/**
 * Macro to skip function execution on error
 * @param[in] function_call: the function to be called with all its parameters
 */
#define SKIP_FUNC_ON_ERROR(function_call) \
  if ( pspio_error_get_last(__func__) == PSPIO_SUCCESS ) { \
    pspio_error_add(function_call, __FILE__, __LINE__, __func__); \
  }

/**
 * Macro to skip a test on error
 * @param[in] function_call: the function to be called with all its parameters
 */
#define SKIP_TEST_ON_ERROR(condition, error_id) \
  if ( (pspio_error_get_last(__func__) == PSPIO_SUCCESS) && !(condition) ) { \
    pspio_error_add(error_id, __FILE__, __LINE__, __func__); \
  }

#endif
