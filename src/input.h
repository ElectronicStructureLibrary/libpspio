/* -*- mode: C; c-basic-offset: 2 -*- */
/* Copyright (C) 2021 Damien Caliste <damien.caliste@cea.fr>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

#ifndef INPUT_H
#define INPUT_H

struct _input_t;
typedef struct _input_t input_t;

int input_new_from_filename(input_t **input, const char *filename);
int input_new_from_file_descriptor(input_t **input, int fd);
int input_new_from_buffer(input_t **input, const char *buffer);

char* input_get_line(input_t *input);
void input_rewind(input_t *input);
void input_push_position(input_t *input);
void input_pop_position(input_t *input);

void input_free(input_t *input);

#endif
