/* Copyright (C) 2011-2016 Joseba Alberdi <alberdi@hotmail.es>
 *                         Matthieu Verstraete <matthieu.jean.verstraete@gmail.com>
 *                         Micael Oliveira <micael.oliveira@mpsd.mpg.de>
 *                         Yann Pouillon <devops@materialsevolution.es>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "pspio_potential.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif


/**********************************************************************
 * Global routines                                                    *
 **********************************************************************/

int pspio_potential_alloc(pspio_potential_t **potential, int np)
{
  assert(potential != NULL);
  assert(*potential == NULL);
  assert(np > 1);

  ALLOC_OR_RETURN( potential );
  DEFER_FUNC_ERROR( pspio_meshfunc_alloc(&(*potential)->v, np) );
  DEFER_FUNC_ERROR( pspio_qn_alloc(&(*potential)->qn) );
  CLEAN_AND_RETURN_ON_DEFERRED_ERROR( potential );

  return PSPIO_SUCCESS;
}

int pspio_potential_init(pspio_potential_t *potential, const pspio_qn_t *qn,
			 const pspio_mesh_t *mesh, const double *vofr)
{
  assert(potential != NULL);
  assert(qn != NULL);
  assert(mesh != NULL);
  assert(vofr != NULL);

  SUCCEED_OR_RETURN( pspio_qn_copy(&potential->qn, qn) );
  SUCCEED_OR_RETURN( pspio_meshfunc_init(potential->v, mesh, vofr) );

  return PSPIO_SUCCESS;
}

int pspio_potential_copy(pspio_potential_t **dst, const pspio_potential_t *src) {
  int np;

  assert(src != NULL);

  np = pspio_mesh_get_np(src->v->mesh);

  if ( *dst == NULL ) {
    SUCCEED_OR_RETURN( pspio_potential_alloc(dst, np) );
  }

  /* 
   * The mesh of the destination potential must have the same number
   * of points as the mesh of the source potential
   */
  if ( pspio_mesh_get_np((*dst)->v->mesh) != np ) {
    pspio_potential_free(*dst);
    *dst = NULL;
    SUCCEED_OR_RETURN(pspio_potential_alloc(dst, np));
  }

  SUCCEED_OR_RETURN( pspio_meshfunc_copy(&(*dst)->v, src->v) );
  SUCCEED_OR_RETURN( pspio_qn_copy(&(*dst)->qn, src->qn) );

  return PSPIO_SUCCESS;
}


void pspio_potential_free(pspio_potential_t *potential)
{
  if (potential != NULL) {
    pspio_meshfunc_free(potential->v);
    pspio_qn_free(potential->qn);
    free(potential);
  }
}


/**********************************************************************
 * Getters                                                            *
 **********************************************************************/

const pspio_qn_t *pspio_potential_get_qn(const pspio_potential_t *potential)
{
  assert(potential != NULL);

  return potential->qn;
}

const pspio_meshfunc_t *pspio_potential_get_v(const pspio_potential_t *potential)
{
  assert(potential != NULL);

  return potential->v;
}


/**********************************************************************
 * Utility routines                                                   *
 **********************************************************************/

int pspio_potential_cmp(const pspio_potential_t *potential1, const
                        pspio_potential_t *potential2) {

  if ((pspio_qn_cmp(potential1->qn, potential2->qn) == PSPIO_DIFF) ||
      (pspio_meshfunc_cmp(potential1->v, potential2->v) == PSPIO_DIFF)) {
    return PSPIO_DIFF;
  } else {
    return PSPIO_EQUAL;
  }
}
